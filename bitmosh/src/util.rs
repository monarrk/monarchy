use std::sync::atomic::{AtomicI32, Ordering};

pub struct AtomicInt {
    atomic: AtomicI32,
}

impl AtomicInt {
    /// New atomic float with initial value `value`.
    pub fn new(value: i32) -> AtomicInt {
        AtomicInt {
            atomic: AtomicI32::new(value),
        }
    }

    /// Get the current value of the atomic float.
    pub fn get(&self) -> i32 {
        self.atomic.load(Ordering::Relaxed)
    }

    /// Set the value of the atomic float to `value`.
    pub fn set(&self, value: i32) {
        self.atomic.store(value, Ordering::Relaxed)
    }
}
