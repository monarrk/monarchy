#[macro_use]
extern crate vst;

mod util;

use util::AtomicInt;

use vst::api::{Events, Supported};
use vst::buffer::AudioBuffer;
use vst::event::Event;
use vst::plugin::{CanDo, Category, Info, Plugin, PluginParameters};
use vst::util::AtomicFloat;

use std::sync::Arc;
use std::f64::consts::PI;

/// Convert the midi note's pitch into the equivalent frequency
#[macro_export]
macro_rules! freq {
    // Default to using 440 for A4
    ( $p:ident ) => {
        (((f64::from($p as i8 - 69)) / 12.).exp2() * 440.00)
    };

    // Dynamic A4 for abnormal tuning / octave manipulation
    ( $p:ident, $a:ident ) => {
        (((f64::from($p as i8 - 69)) / 12.).exp2() * $a)
    };
}

fn period(t: f64) -> f64 {
    1.0 / t
}

enum WaveType {
    Square,
    Sine,
    Saw,
    Triangle,
    Concave,
}

macro_rules! wave_type {
    ( $x:ident ) => {
        int_to_wave_type($x.params.wave.get())
    };
}

fn int_to_wave_type(n: i32) -> WaveType {
    match n {
        0 => WaveType::Square,
        1 => WaveType::Saw,
        2 => WaveType::Triangle,
        3 => WaveType::Concave,
        _ => WaveType::Sine,
    }
}

fn wave_type_to_string(t: WaveType) -> String {
    String::from(match t {
          WaveType::Square => "Square",
          WaveType::Saw => "Saw",
          WaveType::Triangle => "Triangle",
          WaveType::Concave => "Concave",
          _ => "Sine",
    })
}

fn get_wave(t: WaveType, current_note: f64, time: f64, peak: f32) -> f64 {
    match t {
        WaveType::Square => square(current_note, time, peak),
        WaveType::Saw => saw(current_note, time, peak),
        WaveType::Triangle => triangle(current_note, time, peak),
        WaveType::Concave => concave(current_note, time, peak),
        _ => sine(current_note, time),
    }
}

fn square(freq: f64, time: f64, peak: f32) -> f64 {
    (if (2.0 * std::f64::consts::PI * freq * time).sin() > 0.5 { peak} else { -peak }) as f64
}

fn sine(freq: f64, time: f64) -> f64 {
    (time * freq * TAU).sin()
}

fn saw(freq: f64, time: f64, peak: f32) -> f64 {
    (-((2.0 * peak) as f64 / PI)) * (1.0 / ((time * PI) / (1. / freq)).tan()).atan()
}

fn triangle(freq: f64, time: f64, peak: f32) -> f64 {
    let p = period(freq) / 2.0;
    (peak as f64 * 2.0 * (p - ((time % (2.0 * p).abs() - p)).abs())) / p
}

fn concave(freq: f64, time: f64, peak: f32) -> f64 {
    let p = period(freq) / 2.0;
    ((peak as f64 * 2.0 * (p - ((time % (2.0 * p).abs() - p)).abs())) / p).powf(2.0)
}

struct Synth {
    sample_rate: f64,
    time: f64,
    note_duration: f64,
    note: Option<u8>,
    params: Arc<Parameters>,
}

struct Parameters {
    attack: AtomicFloat,
    peak: AtomicFloat,
    octave: AtomicInt,
    wave: AtomicInt,
}

impl Synth {
    fn time_per_sample(&self) -> f64 {
        1.0 / self.sample_rate
    }

    /// Process an incoming midi event.
    ///
    /// The midi data is split up like so:
    ///
    /// `data[0]`: Contains the status and the channel. Source: [source]
    /// `data[1]`: Contains the supplemental data for the message - so, if this was a NoteOn then
    ///            this would contain the note.
    /// `data[2]`: Further supplemental data. Would be velocity in the case of a NoteOn message.
    ///
    /// [source]: http://www.midimountain.com/midi/midi_status.htm
    fn process_midi_event(&mut self, data: [u8; 3]) {
        match data[0] {
            128 => self.note_off(data[1]),
            144 => self.note_on(data[1]),
            _ => (),
        }
    }

    fn note_on(&mut self, note: u8) {
        self.note_duration = 0.0;
        self.note = Some(note)
    }

    fn note_off(&mut self, note: u8) {
        if self.note == Some(note) {
            self.note = None
        }
    }
}

pub const TAU: f64 = PI * 2.0;

impl Default for Synth {
    fn default() -> Synth {
        Synth {
            sample_rate: 34100.0,
            note_duration: 0.0,
            time: 0.0,
            note: None,
            params: Arc::new(Parameters::default()),
        }
    }
}

impl Default for Parameters {
    fn default() -> Parameters {
        Parameters {
            attack: AtomicFloat::new(0.5),
            peak: AtomicFloat::new(0.5),
            octave: AtomicInt::new(4),
            wave: AtomicInt::new(0),
        }
    }
}

impl Plugin for Synth {
    fn get_info(&self) -> Info {
        Info {
            name: "Machine5000".to_string(),
            vendor: "Monarchy".to_string(),
            unique_id: 6667,
            category: Category::Synth,
            inputs: 2,
            outputs: 2,
            parameters: 4,
            initial_delay: 0,
            ..Info::default()
        }
    }

    #[allow(unused_variables)]
    #[allow(clippy::single_match)]
    fn process_events(&mut self, events: &Events) {
        for event in events.events() {
            match event {
                Event::Midi(ev) => self.process_midi_event(ev.data),
                // More events can be handled here.
                _ => (),
            }
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.sample_rate = f64::from(rate);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let samples = buffer.samples();
        let (_, mut outputs) = buffer.split();
        let output_count = outputs.len();
        let per_sample = self.time_per_sample();
        let mut output_sample;

        for sample_idx in 0..samples {
            let time = self.time;
            let note_duration = self.note_duration;

            if let Some(current_note) = self.note {
                // Generate a wave based on self.params.wave
                let peak = self.params.peak.get();
                let o = octave_to_freq(self.params.octave.get());
                let signal = get_wave(wave_type!(self), freq!(current_note, o), time, peak);

                // Apply a quick envelope to the attack of the signal to avoid popping.
                let attack = self.params.attack.get() as f64;
                let alpha = if note_duration < attack { note_duration / attack } else { 1.0 };

                output_sample = (signal * alpha) as f32;

                self.time += per_sample;
                self.note_duration += per_sample;
            } else {
                output_sample = 0.0;
            }

            for buf_idx in 0..output_count {
                let buff = outputs.get_mut(buf_idx);
                buff[sample_idx] = output_sample;
            }
        }
    }

    fn can_do(&self, can_do: CanDo) -> Supported {
        match can_do {
            CanDo::ReceiveMidiEvent => Supported::Yes,
            _ => Supported::Maybe,
        }
    }

    fn get_parameter_object(&mut self) -> Arc<dyn PluginParameters> {
        Arc::clone(&self.params) as Arc<dyn PluginParameters>
    }
}

fn octave_to_freq(n: i32) -> f64 {
    match n {
        0 => 27.50,
        1 => 55.00,
        2 => 110.00,
        3 => 220.00,
        4 => 440.00,
        5 => 880.00,
        6 => 1760.00,
        7 => 3520.00,
        _ => 7040.00,
    }
}

impl PluginParameters for Parameters {
    fn get_parameter(&self, index: i32) -> f32 {
        match index {
            0 => self.attack.get(),
            1 => self.peak.get(),
            2 => octave_to_freq(self.octave.get()) as f32,
            3 => self.wave.get() as f32,
            _ => 0.0,
        }
    }

    fn set_parameter(&self, index: i32, value: f32) {
        match index {
            0 => self.attack.set(value),
            1 => self.peak.set(value),
            2 => self.octave.set((value * 10.0) as i32),
            3 => self.wave.set((value * 10.0) as i32),
            _ => (),
        }
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match index {
            0 => format!("{:.2}", self.attack.get()),
            1 => format!("{:.2}", self.peak.get()),
            2 => format!("{} (A4 {})",
                self.octave.get(),
                octave_to_freq(self.octave.get())),
            3 => wave_type_to_string(int_to_wave_type(self.wave.get())),
            _ => String::new(),
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        match index {
            0 => String::from("Attack"),
            1 => String::from("Peak"),
            2 => String::from("Octave"),
            3 => String::from("Wave Type"),
            _ => String::new(),
        }
    }
}

plugin_main!(Synth);

#[cfg(test)]
mod tests {
    use crate::freq;

    #[test]
    fn midi_to_freq() {
        for i in 0..127 {
            // expect no panics
            let _ = freq!(i);
        }
    }
}
