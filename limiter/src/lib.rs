#[macro_use]
extern crate vst;

use vst::buffer::AudioBuffer;
use vst::plugin::{Category, Info, Plugin, PluginParameters};
use vst::util::AtomicFloat;

use std::sync::Arc;

struct Limiter {
    // Store a handle to the plugin's parameter object.
    params: Arc<LimiterParameters>,
}

/// The plugin's parameter object contains the values of parameters that can be
/// adjusted from the host.  If we were creating an effect that didn't allow the
/// user to modify it at runtime or have any controls, we could omit this part.
///
/// The parameters object is shared between the processing and GUI threads.
/// For this reason, all mutable state in the object has to be represented
/// through thread-safe interior mutability. The easiest way to achieve this
/// is to store the parameters in atomic containers.
struct LimiterParameters {
    limit: AtomicFloat,
    multiplier: AtomicFloat,
}

// All plugins using the `vst` crate will either need to implement the `Default`
// trait, or derive from it.  By implementing the trait, we can set a default value.
// Note that controls will always return a value from 0 - 1.  Setting a default to
// 0.5 means it's halfway up.
impl Default for Limiter {
    fn default() -> Limiter {
        Limiter {
            params: Arc::new(LimiterParameters::default()),
        }
    }
}

impl Default for LimiterParameters {
    fn default() -> LimiterParameters {
        LimiterParameters {
            limit: AtomicFloat::new(0.5),
            multiplier: AtomicFloat::new(0.1),
        }
    }
}

// All plugins using `vst` also need to implement the `Plugin` trait.  Here, we
// define functions that give necessary info to our host.
impl Plugin for Limiter {
    fn get_info(&self) -> Info {
        Info {
            name: "Monarchy Limiter".to_string(),
            vendor: "Monarchy".to_string(),
            unique_id: 243723072,
            version: 1,
            inputs: 2,
            outputs: 2,
            // This `parameters` bit is important; without it, none of our
            // parameters will be shown!
            parameters: 2,
            category: Category::Effect,
            ..Info::default()
        }
    }

    // Here is where the bulk of our audio processing code goes.
    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        // Read the limit from the parameter object
        let limit = self.params.limit.get();

        // First, we destructure our audio buffer into an arbitrary number of
        // input and output buffers.  Usually, we'll be dealing with stereo (2 of each)
        // but that might change.
        for (input_buffer, output_buffer) in buffer.zip() {
            for (input_sample, output_sample) in input_buffer.iter().zip(output_buffer) {
                let n = *input_sample * self.params.multiplier.get();
                
                // clip the values if it's over the limit
                *output_sample = if n > limit {
                    limit
                } else if n < -limit {
                    -limit
                } else {
                  n
                }
            }
        }
    }

    // Return the parameter object. This method can be omitted if the
    // plugin has no parameters.
    fn get_parameter_object(&mut self) -> Arc<dyn PluginParameters> {
        Arc::clone(&self.params) as Arc<dyn PluginParameters>
    }
}

const AMNT: f32 = 10f32;

impl PluginParameters for LimiterParameters {
    // the `get_parameter` function reads the value of a parameter.
    fn get_parameter(&self, index: i32) -> f32 {
        match index {
            0 => self.limit.get() * AMNT,
            1 => self.multiplier.get() * AMNT,
            _ => 0.0,
        }
    }

    // the `set_parameter` function sets the value of a parameter.
    fn set_parameter(&self, index: i32, val: f32) {
        #[allow(clippy::single_match)]
        match index {
            0 => self.limit.set(val),
            1 => self.limit.set(val),
            _ => (),
        }
    }

    // This is what will display underneath our control.  We can
    // format it into a string that makes the most since.
    fn get_parameter_text(&self, index: i32) -> String {
        match index {
            0 => format!("{:.2}", (self.limit.get() - 0.5) * 2f32),
            1 => format!("{:.2}", self.multiplier.get() * AMNT),
            _ => String::new(),
        }
    }

    // This shows the control's name.
    fn get_parameter_name(&self, index: i32) -> String {
        match index {
            0 => "Limit",
            1 => "Multiplier",
            _ => "",
        }
        .to_string()
    }
}

// This part is important! Without it, our plugin won't work.
plugin_main!(Limiter);
