# Monarchy
Free and open source VST plugins and instruments

### Building - OSX
To build the plugins, download the [rust programming language](https://rust-lang.org) and run `cargo build --release`. This will generate a dylib in `./target/release`.

To bundle these dylibs into a VST, use `sys/osx.sh PluginName path/to/libplugin.dylib` and move the generated vst bundle to `/Library/Audio/Plug-Ins/VST` (these are VST 2.4, so do not move them to the VST3 folder).

### Building - Windows
To build the plugins, download the [rust programming language](https://rust-lang.org) and run `cargo build`. This will generate the dll in `.\target\debug`.

Move the DLLs from `.\target\release` to wherever you store them for your DAW and everything (should) work!

### Building - Linux
To build the plugins, download the [rust programming language](https://rust-lang.org) and run `cargo build`. This will generate all the libraries in `./target/debug/`.

To then bundle the libraries into useable VSTs, run `sys/pkg.sh ${plugin_name} ${platform}`. `platform` is the type of computer you're using (for example, `x86_64-linux`).

You can then move that VST bundle to wherever your DAW reads them from and they (should) work!

### Special thanks
- The [RustAudio team's vst-rs library](https://github.com/RustAudio/vst-rs) for an easy to use API
- [BeanBoy](https://beanboy.neocities.org/) for helping me do hard math
